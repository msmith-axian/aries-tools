﻿namespace Tools_GPS
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.txtLatLongStart = new System.Windows.Forms.TextBox();
			this.txtLatLongEnd = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtElapsedTime = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtDateTime = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtWorkerID = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.txtSamples = new System.Windows.Forms.TextBox();
			this.btnGenerate = new System.Windows.Forms.Button();
			this.btnClear = new System.Windows.Forms.Button();
			this.txtResult = new System.Windows.Forms.TextBox();
			this.txtCopyToClipboard = new System.Windows.Forms.Button();
			this.rdoSamples = new System.Windows.Forms.RadioButton();
			this.rdoDistance = new System.Windows.Forms.RadioButton();
			this.txtDistance = new System.Windows.Forms.TextBox();
			this.lblUnitDistance = new System.Windows.Forms.Label();
			this.lblMeasuredDistance = new System.Windows.Forms.Label();
			this.performanceCounter1 = new System.Diagnostics.PerformanceCounter();
			this.lblTimeUnit = new System.Windows.Forms.Label();
			this.btnNextWayPoint = new System.Windows.Forms.Button();
			this.chkIncludeStart = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.performanceCounter1)).BeginInit();
			this.SuspendLayout();
			// 
			// txtLatLongStart
			// 
			resources.ApplyResources(this.txtLatLongStart, "txtLatLongStart");
			this.txtLatLongStart.Name = "txtLatLongStart";
			this.txtLatLongStart.Leave += new System.EventHandler(this.txtLatLongStart_TextChanged);
			// 
			// txtLatLongEnd
			// 
			resources.ApplyResources(this.txtLatLongEnd, "txtLatLongEnd");
			this.txtLatLongEnd.Name = "txtLatLongEnd";
			this.txtLatLongEnd.Leave += new System.EventHandler(this.txtLatLongEnd_TextChanged);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// txtElapsedTime
			// 
			resources.ApplyResources(this.txtElapsedTime, "txtElapsedTime");
			this.txtElapsedTime.Name = "txtElapsedTime";
			this.txtElapsedTime.Leave += new System.EventHandler(this.txtElapsedTime_TextChanged);
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			// 
			// txtDateTime
			// 
			resources.ApplyResources(this.txtDateTime, "txtDateTime");
			this.txtDateTime.Name = "txtDateTime";
			this.txtDateTime.Leave += new System.EventHandler(this.txtDateTime_TextChanged);
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.Name = "label4";
			// 
			// txtWorkerID
			// 
			resources.ApplyResources(this.txtWorkerID, "txtWorkerID");
			this.txtWorkerID.Name = "txtWorkerID";
			// 
			// label5
			// 
			resources.ApplyResources(this.label5, "label5");
			this.label5.Name = "label5";
			// 
			// label6
			// 
			resources.ApplyResources(this.label6, "label6");
			this.label6.Name = "label6";
			// 
			// txtSamples
			// 
			resources.ApplyResources(this.txtSamples, "txtSamples");
			this.txtSamples.Name = "txtSamples";
			this.txtSamples.Leave += new System.EventHandler(this.txtSamples_TextChanged);
			// 
			// btnGenerate
			// 
			resources.ApplyResources(this.btnGenerate, "btnGenerate");
			this.btnGenerate.Name = "btnGenerate";
			this.btnGenerate.UseVisualStyleBackColor = true;
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			// 
			// btnClear
			// 
			resources.ApplyResources(this.btnClear, "btnClear");
			this.btnClear.Name = "btnClear";
			this.btnClear.UseVisualStyleBackColor = true;
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// txtResult
			// 
			resources.ApplyResources(this.txtResult, "txtResult");
			this.txtResult.Name = "txtResult";
			// 
			// txtCopyToClipboard
			// 
			resources.ApplyResources(this.txtCopyToClipboard, "txtCopyToClipboard");
			this.txtCopyToClipboard.Name = "txtCopyToClipboard";
			this.txtCopyToClipboard.UseVisualStyleBackColor = true;
			this.txtCopyToClipboard.Click += new System.EventHandler(this.txtCopyToClipboard_Click);
			// 
			// rdoSamples
			// 
			resources.ApplyResources(this.rdoSamples, "rdoSamples");
			this.rdoSamples.Checked = true;
			this.rdoSamples.Name = "rdoSamples";
			this.rdoSamples.TabStop = true;
			this.rdoSamples.UseVisualStyleBackColor = true;
			this.rdoSamples.CheckedChanged += new System.EventHandler(this.rdoSamples_CheckedChanged);
			// 
			// rdoDistance
			// 
			resources.ApplyResources(this.rdoDistance, "rdoDistance");
			this.rdoDistance.Name = "rdoDistance";
			this.rdoDistance.UseVisualStyleBackColor = true;
			this.rdoDistance.CheckedChanged += new System.EventHandler(this.rdoDistance_CheckedChanged);
			// 
			// txtDistance
			// 
			resources.ApplyResources(this.txtDistance, "txtDistance");
			this.txtDistance.Name = "txtDistance";
			this.txtDistance.ReadOnly = true;
			this.txtDistance.Leave += new System.EventHandler(this.txtDistance_TextChanged);
			// 
			// lblUnitDistance
			// 
			resources.ApplyResources(this.lblUnitDistance, "lblUnitDistance");
			this.lblUnitDistance.Name = "lblUnitDistance";
			// 
			// lblMeasuredDistance
			// 
			resources.ApplyResources(this.lblMeasuredDistance, "lblMeasuredDistance");
			this.lblMeasuredDistance.Name = "lblMeasuredDistance";
			// 
			// lblTimeUnit
			// 
			resources.ApplyResources(this.lblTimeUnit, "lblTimeUnit");
			this.lblTimeUnit.Name = "lblTimeUnit";
			// 
			// btnNextWayPoint
			// 
			resources.ApplyResources(this.btnNextWayPoint, "btnNextWayPoint");
			this.btnNextWayPoint.Name = "btnNextWayPoint";
			this.btnNextWayPoint.UseVisualStyleBackColor = true;
			this.btnNextWayPoint.Click += new System.EventHandler(this.btnNextWayPoint_Click);
			// 
			// chkIncludeStart
			// 
			resources.ApplyResources(this.chkIncludeStart, "chkIncludeStart");
			this.chkIncludeStart.Checked = true;
			this.chkIncludeStart.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkIncludeStart.Name = "chkIncludeStart";
			this.chkIncludeStart.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.chkIncludeStart);
			this.Controls.Add(this.btnNextWayPoint);
			this.Controls.Add(this.lblTimeUnit);
			this.Controls.Add(this.lblMeasuredDistance);
			this.Controls.Add(this.lblUnitDistance);
			this.Controls.Add(this.txtDistance);
			this.Controls.Add(this.rdoDistance);
			this.Controls.Add(this.rdoSamples);
			this.Controls.Add(this.txtCopyToClipboard);
			this.Controls.Add(this.txtResult);
			this.Controls.Add(this.btnClear);
			this.Controls.Add(this.btnGenerate);
			this.Controls.Add(this.txtSamples);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.txtWorkerID);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtDateTime);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtElapsedTime);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtLatLongEnd);
			this.Controls.Add(this.txtLatLongStart);
			this.Name = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.performanceCounter1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtLatLongStart;
		private System.Windows.Forms.TextBox txtLatLongEnd;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtElapsedTime;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtDateTime;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtWorkerID;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtSamples;
		private System.Windows.Forms.Button btnGenerate;
		private System.Windows.Forms.Button btnClear;
		private System.Windows.Forms.TextBox txtResult;
		private System.Windows.Forms.Button txtCopyToClipboard;
		private System.Windows.Forms.RadioButton rdoSamples;
		private System.Windows.Forms.RadioButton rdoDistance;
		private System.Windows.Forms.TextBox txtDistance;
		private System.Windows.Forms.Label lblUnitDistance;
		private System.Windows.Forms.Label lblMeasuredDistance;
		private System.Diagnostics.PerformanceCounter performanceCounter1;
		private System.Windows.Forms.Label lblTimeUnit;
		private System.Windows.Forms.Button btnNextWayPoint;
		private System.Windows.Forms.CheckBox chkIncludeStart;
	}
}

