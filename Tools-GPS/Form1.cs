﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tools_GPS
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}
		
		private void btnGenerate_Click(object sender, EventArgs e)
		{
			string lineResult = "";
			string latLongStart = txtLatLongStart.Text;
			string latLongEnd = txtLatLongEnd.Text;
			string latPartial = "";
			string longPartial = "";
			string[] strLatLongStart = latLongStart.Split(',');
			string[] strLatLongEnd = latLongEnd.Split(',');
			double latStart = Convert.ToDouble(strLatLongStart[0]);
			double longStart = Convert.ToDouble(strLatLongStart[1]);
			double latEnd = Convert.ToDouble(strLatLongEnd[0]);
			double longEnd = Convert.ToDouble(strLatLongEnd[1]);
			double latDiff = 0;
			double longDiff = 0;
			double measuredDistance = GetMeasuredDistance("feet", latStart, latEnd, longStart, longEnd);

			int samples = int.Parse(txtSamples.Text);		
			int secondOffset = 1;
			int roundValue = 6;
			DateTime startDate = Convert.ToDateTime(txtDateTime.Text);
			int elapsedTime = int.Parse(txtElapsedTime.Text);
			DateTime endDate = startDate.AddSeconds(elapsedTime);
			if (samples == 1)
			{
				secondOffset = 0;
			} else
			{
				secondOffset = elapsedTime / samples;
			}

			txtResult.Text = "{";
			txtResult.AppendText("\r\n\t\"userid\": \"" + txtWorkerID.Text + "\",");
			txtResult.AppendText("\r\n\t\"positions\": [");
			for (int i = 0; i <= samples; i++)
			{
				if(i==0 && chkIncludeStart.Checked)
				{
					lineResult = GetDataStringSegment(
								txtWorkerID.Text,
								Math.Round(latStart, roundValue).ToString(),
								Math.Round(longStart, roundValue).ToString(),
								startDate.ToUniversalTime(),
								true
					);
					txtResult.AppendText(lineResult);

				} else if (i==samples)
				{
					lineResult = GetDataStringSegment(
								txtWorkerID.Text, 
								Math.Round(latEnd, roundValue).ToString(),
								Math.Round(longEnd, roundValue).ToString(),
								endDate.ToUniversalTime(),
								false
					);						
					txtResult.AppendText(lineResult);
				} else if (i > 0)
				{
					DateTime partialTimeValue = startDate.AddSeconds(i * secondOffset).ToUniversalTime();

					// Calculate Lat with direction
					if (latStart > latEnd)
					{
						latDiff = (latStart - latEnd) / Convert.ToDouble(samples);
						latPartial = Math.Round(latStart - (latDiff * i), roundValue).ToString();

					} else if (latStart < latEnd)
					{
						latDiff = (latEnd - latStart) / Convert.ToDouble(samples);
						latPartial = Math.Round(latStart + (latDiff * i), roundValue).ToString();
					}

					// Calculate Long with direction
					if (longStart > longEnd)
					{
						longDiff = (longStart - longEnd) / Convert.ToDouble(samples);
						longPartial = Math.Round(longStart - (longDiff * i),roundValue).ToString();

					}
					else if (longStart < longEnd)
					{
						longDiff = (longEnd - longStart) / Convert.ToDouble(samples);
						longPartial = Math.Round(longStart + (longDiff * i),roundValue).ToString();
					}

					lineResult = GetDataStringSegment(txtWorkerID.Text, latPartial, longPartial, partialTimeValue,true);
					txtResult.AppendText(lineResult);
				}

				//result += lineResult;

			}
			txtResult.AppendText("\r\n\t]\r\n}");  // Append the closing tail
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			txtResult.Text = null;
			txtDateTime.Text = null;
			txtElapsedTime.Text = null;
			txtLatLongStart.Text = null;
			txtLatLongEnd.Text = null;
			rdoDistance.Checked = false;
			rdoSamples.Checked = true;
			txtSamples.Text = null;
			txtSamples.ReadOnly = false;
			txtSamples.Enabled = false;
			txtDistance.Text = null;
			txtDistance.ReadOnly = true;
			txtDistance.Enabled = false;
			lblMeasuredDistance.Text = " - ";
			txtWorkerID.Text = null;

		}

		private void rdoSamples_CheckedChanged(object sender, EventArgs e)
		{
			txtSamples.ReadOnly = false;
			txtDistance.ReadOnly = true;
			txtDistance.Text = null;
		}

		private void rdoDistance_CheckedChanged(object sender, EventArgs e)
		{
			txtSamples.ReadOnly = true;
			txtDistance.ReadOnly = false;
			txtSamples.Text = null;
		}

		private double GetMeasuredDistance(string unit, double lat1, double lat2, double long1, double long2)
		{
			double measuredDistance = 0;
			double radius = 0;

			if (unit.ToLower() == "feet")
			{
				radius = 20903520;
			}
			else if (unit.ToLower() == "meter")
			{
				radius = 6371000;
			}

			measuredDistance =
				Math.Acos(Math.Sin(lat1 * Math.PI / 180) * Math.Sin(lat2 * Math.PI / 180) +
								 (Math.Cos(lat1 * Math.PI / 180) * Math.Cos(lat2 * Math.PI / 180) * Math.Cos(long2 * Math.PI / 180 - long1 * Math.PI / 180))
				) * radius;


			return measuredDistance;
		}

		private double GetMeasuredDistance(string unit, string latLongStart, string latLongEnd)
		{
			double measuredDistance = 0;
			double radius = 0;
			string[] strLatLongStart = latLongStart.Split(',');
			string[] strLatLongEnd = latLongEnd.Split(',');

			double lat1 = Convert.ToDouble(strLatLongStart[0]);
			double long1 = Convert.ToDouble(strLatLongStart[1]);
			double lat2 = Convert.ToDouble(strLatLongEnd[0]);
			double long2 = Convert.ToDouble(strLatLongEnd[1]);



			if (unit.ToLower() == "feet")
			{
				radius = 20903520;
			}
			else if (unit.ToLower() == "meter")
			{
				radius = 6371000;
			}

			measuredDistance =
				Math.Acos(Math.Sin(lat1 * Math.PI / 180) * Math.Sin(lat2 * Math.PI / 180) +
								 (Math.Cos(lat1 * Math.PI / 180) * Math.Cos(lat2 * Math.PI / 180) * Math.Cos(long2 * Math.PI / 180 - long1 * Math.PI / 180))
				) * radius;


			return measuredDistance;
		}

		private void txtSamples_TextChanged(object sender, EventArgs e)
		{
			double samples;
			bool isNumber = Double.TryParse(txtSamples.Text, out samples);

			if (isNumber)
			{
				double distance = GetMeasuredDistance("feet", txtLatLongStart.Text, txtLatLongEnd.Text);
				txtDistance.Text = Math.Round(distance / samples).ToString();
			} else if (txtSamples.TextLength != 0)
			{
				txtSamples.Clear();
				txtDistance.Clear();
				MessageBox.Show("Please Enter a Valid Number for Samples.", "Invalid Number");
			}
		}

		private void txtDistance_TextChanged(object sender, EventArgs e)
		{
			double sampleDistance;
			bool isNumber = Double.TryParse(txtDistance.Text, out sampleDistance);


			if (isNumber)
			{
				double distance = GetMeasuredDistance("feet", txtLatLongStart.Text, txtLatLongEnd.Text);
				txtSamples.Text = Math.Round(distance / sampleDistance).ToString();
			}
			else if (txtDistance.TextLength != 0) {
				txtSamples.Clear();
				txtDistance.Clear();
				MessageBox.Show("Please Enter a Valid Number for Distance.", "Invalid Number");
			}


		}

		private void txtElapsedTime_TextChanged(object sender, EventArgs e)
		{
			int elapsedTime;
			bool isNumber = int.TryParse(txtElapsedTime.Text, out elapsedTime);

			if (!isNumber && txtElapsedTime.TextLength != 0)
			{
				txtElapsedTime.Clear();
				MessageBox.Show("Please Enter a Valid Number for Elapsed Time.", "Invalid Number");
			}
		}

		private void txtDateTime_TextChanged(object sender, EventArgs e)
		{
			DateTime enteredDate;
			bool isDate = DateTime.TryParse(txtDateTime.Text, out enteredDate);

			if (!isDate && txtDateTime.TextLength != 0)
			{
				txtDateTime.Clear();
				MessageBox.Show("Please Enter a Valid Date.", "Invalid Number");
			}
		}

		private void txtLatLongStart_TextChanged(object sender, EventArgs e)
		{

			if (txtLatLongStart.Text != null && txtLatLongStart.Text != "" && 
					txtLatLongEnd.Text != null && txtLatLongEnd.Text != "" &&
					txtLatLongStart.Text.Split(',').Length == 2 &&
					txtLatLongEnd.Text.Split(',').Length == 2)
			{
				rdoDistance.Enabled = true;
				rdoSamples.Enabled = true;
				txtDistance.Enabled = true;
				txtSamples.Enabled = true;
				lblMeasuredDistance.Text = Math.Round(GetMeasuredDistance("feet", txtLatLongStart.Text, txtLatLongEnd.Text)).ToString() + " ft";
			} else if (txtLatLongStart.Text == null || txtLatLongStart.Text == "" ||
								 txtLatLongEnd.Text == null || txtLatLongEnd.Text == "" || 
								 txtLatLongStart.Text.Split(',').Length != 2 ||
								 txtLatLongEnd.Text.Split(',').Length != 2)
			{
				rdoDistance.Enabled = false;
				rdoSamples.Enabled = false;
				txtDistance.Enabled = false;
				txtSamples.Enabled = false;
				lblMeasuredDistance.Text = " - ";
			}
		}

		private void txtLatLongEnd_TextChanged(object sender, EventArgs e)
		{
			if (txtLatLongStart.Text != null && txtLatLongStart.Text != "" &&
					txtLatLongEnd.Text != null && txtLatLongEnd.Text != "" &&
					txtLatLongEnd.Text.Split(',').Length == 2)
			{
				rdoDistance.Enabled = true;
				rdoSamples.Enabled = true;
				txtDistance.Enabled = true;
				txtSamples.Enabled = true;
				lblMeasuredDistance.Text = Math.Round(GetMeasuredDistance("feet", txtLatLongStart.Text, txtLatLongEnd.Text)).ToString() + " ft";
			} else if (txtLatLongStart.Text == null || txtLatLongStart.Text == "" ||
								 txtLatLongEnd.Text == null || txtLatLongEnd.Text == "" ||
								 txtLatLongEnd.Text.Split(',').Length != 2)
			{
				rdoDistance.Enabled = false;
				rdoSamples.Enabled = false;
				txtDistance.Enabled = false;
				txtSamples.Enabled = false;
				lblMeasuredDistance.Text = " - ";
			}
		}

		private string GetDataStringSegment(string user, string segmentLat, string segmentLong, DateTime segmentDate, bool boolComma)
		{
			string stringSegment = "";
			stringSegment = "\r\n" + "\t{" + "\r\n" +
						"\t\t\"userid\": \"" + user + "\",\r\n" +
						"\t\t\"latitude\": " + segmentLat + ",\r\n" +
						"\t\t\"longitude\": " + segmentLong + ",\r\n" +
						"\t\t\"date\": \"" + String.Format("{0:yyyy-MM-dd}", segmentDate) + "T" + String.Format("{0:HH:mm:ss.fff}", segmentDate) + "Z\"\r\n\t}";

			if(boolComma)
			{
				stringSegment += ",";
			}


			return stringSegment;
		}

		private void txtCopyToClipboard_Click(object sender, EventArgs e)
		{
			Clipboard.SetText(txtResult.Text);
		}

		private void btnNextWayPoint_Click(object sender, EventArgs e)
		{
			txtLatLongStart.Text = txtLatLongEnd.Text;
			txtDateTime.Text = DateTime.Parse(txtDateTime.Text).AddSeconds(Double.Parse(txtElapsedTime.Text)).ToString();
			chkIncludeStart.Checked = false;
			lblMeasuredDistance.Text = " - ";
			txtLatLongEnd.Clear();
			txtResult.Clear();
		}

		/*
		private void chkIncludeStart_CheckedChanged(object sender, EventArgs e)
		{

		}
		*/
	}
}
